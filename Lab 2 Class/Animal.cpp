#include "Animal.h"

Animal::Animal() {
	weight = 0.0;
	legAmount = 0;
	name = "";
}

Animal::Animal(double w, int leg, string nam, bool land) {
	setWeight(w);
	setLegAmount(leg);
	setName(nam);
	setIsLandAnimal(land);
}

Animal::~Animal() {
	//destroy
}

void Animal::setWeight(double w) {
	if (w > 0) {
		weight = w;
	}
	else {
		//throw error
	}

}

double Animal::getWeight() {
	return weight;
}

void Animal::setLegAmount(int leg) {
	if (leg <= 0) {
		legAmount = leg;
	}
	else {
		//throw error
	}
}

int Animal::getLegAmount() {
	return legAmount;
}

void Animal::setName(string n) {
	name = n;
}

string Animal::getName() {
	return name;
}

void Animal::setIsLandAnimal(bool b) {
	isLandAnimal = b;
}

bool Animal::getIsLandAnimal() {
	return isLandAnimal;
}

void Animal::toString() {
	//cout << (Animal::getName() + " weight: " + Animal::getWeight() + ", leg amount: " + Animal::getLegAmount() + ", is land: " + Animal::getIsLandAnimal() + "\n"); //This is not working properly

	cout << Animal::getName();
	cout << " weight: ";
	cout << Animal::getWeight();
	cout << ", leg amount: ";
	cout << Animal::getLegAmount();
	cout << ", is land: ";
	cout << Animal::getIsLandAnimal();
	cout << "\n";
}
