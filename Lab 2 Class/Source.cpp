#include<iostream>
#include <chrono>
#include <thread>
#include <algorithm>
#include "Animal.h"

using namespace std;

void menu();
void delay(int);
bool selectMenuOption();
void addAnimal();
void viewAnimals();

char input;
Animal animals[99];
int sizeOfArray = 0;

int main() {

	while (true) {
		menu();
		if (selectMenuOption()) {
			cout << "\nShutting Down!\n";
			delay(1);
			return 0;
		}
	}


	return 0;
}

void menu() {
	system("CLS");
	std::cout << "Menu: Select option, then press Enter\n\n";
	std::cout << "1. Add Animal\n";
	std::cout << "2. View all Animals\n";
	std::cout << "3. Exit\n\n";

}

void delay(int i) {
	std::chrono::seconds dura(i);
	std::this_thread::sleep_for(dura);
}

bool selectMenuOption() {
	cin >> input;
	switch (input)
	{
	case '1':
		addAnimal();
		return false;
		break;

	case '2':
		viewAnimals();
		return false;
		break;

	case '3':
		return true;
		break;

	default:
		system("CLS");
		cout << "Invalid input\n";
		input = '0'; //Cleans the input variable to avoid an incorrect behavior
		delay(1);
		break;
	}
	return false;
}

void addAnimal() {
	double weight;
	int legAmount;
	string name;
	bool isLandAnimal;


	cout << "Enter animal weight, then press \'Enter\' key\n";
	cin >> weight;
	while (cin.fail() || weight < 0) {
		cout << "Invalid input. Input is not a number!\n";
		weight = 0;
		delay(1);
		cin.clear();
		cin.ignore(10000, '\n');
		cin >> weight;
	}


	cout << "Enter animal leg amount, then press \'Enter\' key\n";
	cin >> legAmount;
	while (cin.fail() || legAmount < 0) {
		cout << "Invalid input. Input is not a valid number!\n";
		legAmount = 0;
		delay(1);
		cin.clear();
		cin.ignore(10000, '\n');
		cin >> legAmount;
	}

	
	cout << "Enter animal name, then press \'Enter\' key\n";
	cin >> name;

	cout << "Does the animal live on land? Type \'y\' for YES or \'n\' for NO , then press \'Enter\' key\n";
	cin >> input;
	while (input != 'y' && input != 'n') {
		cout << "Not a valid option, try again!\n";
		input = '0';
		cin.clear();
		cin.ignore(10000, '\n');
		cin >> input;
	}
	if (input == 'y') {
		isLandAnimal = true;
	}
	else {
		isLandAnimal = false;
	}

	Animal newAnimal = Animal::Animal(weight, legAmount, name, isLandAnimal); //Somehow the constructor doesn't assign the correct value to parameter 'legAmount'
	animals[sizeOfArray++] = newAnimal;
}

void viewAnimals() {
	if (sizeOfArray==0) {
		cout << "No Animals to Display. Must add at least 1 animal";
		delay(1);
		return;
	}
	for (int i = 0; i < sizeOfArray; i++) {
		animals[i].toString();
	}
	cout << "\nType in any key then press Enter to continue.\n";
	cin >> input; // Used in order to wait for the user to finish viewing grades.
	input = '0'; //Cleans the input variable to avoid an incorrect behavior

}