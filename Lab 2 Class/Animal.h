#pragma once
#include<string>
#include<iostream>
using namespace std;

class Animal
{
public:
	Animal();
	Animal(double, int, string, bool);
	~Animal();

private:
	double weight;
	int legAmount;
	string name;
	bool isLandAnimal;

public:
	void setWeight(double);
	double getWeight();
	void setLegAmount(int);
	int getLegAmount();
	void setName(string);
	string getName();
	void setIsLandAnimal(bool);
	bool getIsLandAnimal();
	void toString();
};

